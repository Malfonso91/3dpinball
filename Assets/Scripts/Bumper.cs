﻿using UnityEngine;
using System.Collections;

public class Bumper : MonoBehaviour
{

    public float Force = 100f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Ball")
        {
            if (other.rigidbody.velocity.magnitude < 15f)
            {
                float x = other.rigidbody.velocity.x > 0 ? Force : -Force;
                float z = other.rigidbody.velocity.z > 0 ? Force : -Force;
                other.rigidbody.AddForce(new Vector3(x, 0f, z));
            }
        }
    }
}