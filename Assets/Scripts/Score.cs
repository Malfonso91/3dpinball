﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {
    private int bumper;
    private int target;


	// Use this for initialization
	void Start () {
        bumper = 200;
        target = 500;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bumper")
        {
            bumper++;
        }
        else if (collision.gameObject.tag == "Target")
        {
            target++;
        }
    }
}
