﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour
{
    public GameObject ball;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                ball.SetActive(false);
                if (hit.transform.name == "Coin")
                {
                    //Debug.Log("object clicked!");
                    for(int i = 0; i < 1; i++)
                    {
                        ball.SetActive(true);
                        Instantiate(ball);
                        ball.transform.position = new Vector3(3, -0.08f, -1);
                    }
                }
            }
        }

    }
}
