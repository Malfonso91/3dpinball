﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lives : MonoBehaviour {
    private int lives;

	// Use this for initialization
	void Start () {
        lives = 3;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Death")
        {
            for (int i = 0; i < lives; i++)
            {
                lives--;
            }
            if (lives == 0)
            {
                Debug.Log("GAME OVER");
            }

        }
    }
}
