﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
    public GameObject target;
    private float vectorX;
    private float vectorY;
    private float vectorZ;
    private Vector3 startPos;


	// Use this for initialization
	void Start ()
    {
        vectorX = -2;
        vectorY = -0.57f;
        vectorZ = 4.11f;
        startPos = target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Target")
        {
            collision.transform.localPosition = new Vector3(vectorX, vectorY, vectorZ);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Target")
        {
            //collision.transform.position = startPos;
        }
    }
}
